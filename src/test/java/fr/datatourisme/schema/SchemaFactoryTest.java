/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.schema;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SequenceWriter;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;
import org.junit.Assert;
import org.junit.Test;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by blaise on 09/07/17.
 */
public class SchemaFactoryTest {
    @Test
    public void createSchema() throws Exception {

        List<String> files = new ArrayList<>();
        files.add("submodules/ontology/datatourisme.ttl");
        files.add("submodules/ontology-extra/datatourisme-extras.ttl");

        SchemaFactory factory = new SchemaFactory(files);
        Schema schema = factory.createSchema();

        //schema.getTypes().get(":PointOfInterest").getFields().forEach(System.out::println);

//        System.out.println(" === TYPES === ");
//        schema.getTypes().forEach((s, t) -> System.out.println(s));
//
//        System.out.println(" === FIELDS === ");
//        schema.getFields().forEach((s, t) -> System.out.println(s));


        // parse into yaml
        YAMLFactory yamlFactory = new YAMLFactory()
            .disable(YAMLGenerator.Feature.WRITE_DOC_START_MARKER)
            .disable(YAMLGenerator.Feature.MINIMIZE_QUOTES);

        ObjectMapper mapper = new ObjectMapper(yamlFactory);
        String yamlString = mapper.writeValueAsString(schema)
            .replace("__empty__", "\"\"");
        System.out.print(yamlString);

//        ObjectWriter writer = mapper.writer();
//        SequenceWriter sw = writer.writeValues(System.out);
//        sw.write(schema);
    }
}