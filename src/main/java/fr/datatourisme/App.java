/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;
import fr.datatourisme.schema.Schema;
import fr.datatourisme.schema.SchemaFactory;
import org.apache.commons.cli.*;

import java.util.List;

public class App 
{
    public static void main( String[] args )
    {
        Options options = new Options();
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
            List<String> inputFiles = cmd.getArgList();
            if(inputFiles.size() < 1) {
                throw new MissingArgumentException("Vous devez spécifier au moins un fichier d'ontologie !");
            }

            // build schema
            SchemaFactory factory = new SchemaFactory(inputFiles);
            Schema schema = factory.createSchema();

            //schema.getTypes().keySet().forEach(System.out::println);

            // parse into yaml
            YAMLFactory yamlFactory = new YAMLFactory()
                .disable(YAMLGenerator.Feature.WRITE_DOC_START_MARKER)
                .disable(YAMLGenerator.Feature.MINIMIZE_QUOTES);

            ObjectMapper mapper = new ObjectMapper(yamlFactory);
//            ObjectWriter writer = mapper.writer();
//            SequenceWriter sw = writer.writeValues(System.out);
//            sw.write(schema);

            String yamlString = mapper.writeValueAsString(schema)
                    .replace("__empty__", "\"\"");
            System.out.print(yamlString);

        } catch (Exception e) {
            System.err.println(e.getMessage());
//            HelpFormatter formatter = new HelpFormatter();
//            System.out.println(e.getMessage());
//            formatter.printHelp("graphql-schema-generator", options);
            System.exit(1);
        }
    }
}
